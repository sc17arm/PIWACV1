# PIWACV1

This project was created in Unity 2018.4.16. Some of the assets used will not be 
compatible with other versions of Unity.

The project cosists of seven different scenes, that make up the six scenarios.
Add all scenes to the hierarchy tab (standardScene, HonkScene, LorryScene, 
20mphScene, 40mphScene and shortDistance).
Each scenario can be accessed by loading the standardScene and one of the other 
scenes. 
To unload or load a scene, righclick the scene in the hierarchy tab and click 
load or unload. 
Click the play button at the top of the editor to run the scenario. To exit the 
scenario press esc and then the play button again.
To turn on or off logging go into the CarWaypointBased and check or uncheck the 
checkbox next to the logging scripts.