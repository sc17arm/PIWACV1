using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]

    public class logLocation : MonoBehaviour
    {
        private CarController m_CarController;

        // Start is called before the first frame update
        void Start()
        {
            m_CarController = GetComponent<CarController>();
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log("(" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + ") Car location x: " + transform.position.x + " y: " + transform.position.z);
        }
    }
}