using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]

    public class logspeed : MonoBehaviour
    {
        [SerializeField] private Transform m_Target;

        private CarController m_CarController;

        // Start is called before the first frame update
        void Start()
        {
            m_CarController = GetComponent<CarController>();
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 delta = m_Target.position - transform.position;

            if(delta[2] < 1 && delta[2] > -1)
            {
                Debug.Log("(" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + ") Speed of car " + m_CarController.CurrentSpeed + "mph");
            }
        }

        public void SetTarget(Transform target)
        {
            m_Target = target;
        }
    }
}
