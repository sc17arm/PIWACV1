using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]

    public class honk : MonoBehaviour
    {
        [SerializeField] private Transform m_Target;

        private CarController m_CarController;
        private AudioSource honkSound;
        private Boolean honked;

        // Start is called before the first frame update
        void Start()
        {
            m_CarController = GetComponent<CarController>();
            honkSound = GetComponent<AudioSource>();
            honked = false;
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 delta = m_Target.position - transform.position;

            if(delta[2] < 1 && delta[2] > -1 && honked == false)
            {
                honkSound.Play();
                Debug.Log("(" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + ") Honk!");
                honked = true;
            }
        }

        public void SetTarget(Transform target)
        {
            m_Target = target;
        }
    }
}