using System.Collections;
using UnityStandardAssets.Utility;
using UnityEngine;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (CharacterController))]

    public class logPedestrian : MonoBehaviour
    {
        private CharacterController m_CharacterController;

        // Start is called before the first frame update
        void Start()
        {
            m_CharacterController = GetComponent<CharacterController>();
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log("(" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + ") Pedestrian location x: " + transform.position.x + " y: " + transform.position.z);
        }

    }
}