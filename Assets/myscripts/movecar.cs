﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (CarController))]

    public class movecar : MonoBehaviour
    {
        [SerializeField] private Transform m_Target; 
        private bool m_Driving;

        private CarController m_CarController;
        private Rigidbody m_Rigidbody;

        // Start is called before the first frame update
        void Start()
        {
            m_CarController = GetComponent<CarController>();
            m_Rigidbody = GetComponent<Rigidbody>();
            m_Driving = true;
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (m_Target == null || !m_Driving)
            {
                m_CarController.Move(0, 0, -1f, 1f);
            }
            else 
            {
                float desiredSpeed = m_CarController.MaxSpeed;


                Vector3 delta = m_Target.position - transform.position;

                m_CarController.Move(0, desiredSpeed, 0, 0f);

                if (delta[2] < 1 && delta[2] > -1)
                {
                    m_Driving = false;
                }
            }
        }

        public void SetTarget(Transform target)
        {
            m_Target = target;
        }

        private void OnCollisionEnter(Collision col)
        {
            m_CarController.Move(0, 0, -1f, 1f);
            m_Driving = false;
            Debug.Log("(" + System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + ") Collision!");
        }
    }
}
